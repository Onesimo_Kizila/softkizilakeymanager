﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace softkizilakeymanager
{
    public partial class frmValidator : Form
    {
        public string validuser1 { get; set; } = "onesimonuneskizila@gmail.com";
        public string validuser2 { get; set; } = "hackerboy1998s@gmail.com";
        public string validuser3 { get; set; } = "genesys2013s@gmail.com";
        public string NomeEmpresa { get; set; }
        public string Code { get; set; }
        public string IdEmpresa { get; set; }
        public string Website { get; set; }
        public string Endereco { get; set; }
        public string Email { get; set; }
        public string Data_h { get; set; }
        public string Data_f { get; set; }
        public string Anos { get; set; }
        public string Images { get; set; }
        public string LogoCount { get; set; }
        public string Descr { get; set; }
        public DB convert { get; set; }

        public frmValidator()
        {
            convert = new DB();
            InitializeComponent();

            validuser1 = convert.criptografar(validuser1);
            validuser2 = convert.criptografar(validuser2);
            validuser3 = convert.criptografar(validuser3);

            //for(var i = 0; i <= 5; i++)
            //{
            //    cbAnos.Items.Add(i);
            //}
        }

        public byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            //using (var ms = new MemoryStream())
            //{
            //    imageIn.Save(ms, imageIn.RawFormat);
            //    return ms.ToArray();
            //}
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            return ms.GetBuffer();
        }

        private void btnLisenca_Click(object sender, EventArgs e)
        {
            NomeEmpresa = convert.criptografar(txtEmpresa.Text);
            Code = convert.criptografar(txtCodigo.Text);
            IdEmpresa = convert.criptografar(txtCodigo.Text);
            Website = convert.criptografar(txtWebSite.Text);
            Endereco = convert.criptografar(txtEndereco.Text);
            Email = convert.criptografar(txtEmail.Text);
            Data_h = convert.criptografar(DateTime.Now.ToString("dd/MM/yyyy"));
            Data_f = convert.criptografar(DateTime.Now.AddYears(Convert.ToInt32(Convert.ToString(cbAnos.SelectedIndex+1).Trim())).ToString("dd/MM/yyyy"));
            Anos = convert.criptografar(Convert.ToString(cbAnos.SelectedIndex + 1).Trim());
            var byteimage = ImageToByteArray(picLogo.Image);
            Images = Convert.ToBase64String(byteimage);
            LogoCount = convert.criptografar(Convert.ToString(byteimage.Length));

            var filelocation = Directory.GetCurrentDirectory()+("\\licença para empresa {0}.lic").Replace("{0}", txtEmpresa.Text);

            using (StreamWriter file =
           new StreamWriter(filelocation))
            {
                file.WriteLine(validuser1);
                file.WriteLine(IdEmpresa);
                file.WriteLine(Code);
                file.WriteLine(NomeEmpresa);
                file.WriteLine(Endereco);
                file.WriteLine(Email);
                file.WriteLine(validuser2);
                file.WriteLine(Website);
                file.WriteLine(Data_h);
                file.WriteLine(Data_h);
                file.WriteLine(Data_f);
                file.WriteLine(Anos);
                file.WriteLine(Convert.ToBase64String(ImageToByteArray(picLogo.Image)));
                file.WriteLine(LogoCount);
                file.WriteLine(validuser3);
            }
            MessageBox.Show("Ficheiro de Licença Gerada com sucesso!", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                // display image in picture box  
               picLogo.Image = ResizeImage(open.FileName, 100, 100, true);
                // image file path  
            }
        }
        private void Limpar()
        {
            txtEmpresa.Text = string.Empty;
            txtCodigo.Text = string.Empty;
            txtWebSite.Text = string.Empty;
            txtEndereco.Text = string.Empty;
            txtEmail.Text = string.Empty;
            cbAnos.SelectedIndex = 0;
            picLogo.Image = null;
        }
        public static Image ResizeImage(string originalFile, int newWidth, int maxHeight, bool onlyResizeIfWider)
        {
            Image fullsizeImage = Image.FromFile(originalFile);

            // Prevent using images internal thumbnail
            fullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            fullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            if (onlyResizeIfWider)
            {
                if (fullsizeImage.Width <= newWidth)
                {
                    newWidth = fullsizeImage.Width;
                }
            }

            int newHeight = fullsizeImage.Height * newWidth / fullsizeImage.Width;
            if (newHeight > maxHeight)
            {
                // Resize with height instead
                newWidth = fullsizeImage.Width * maxHeight / fullsizeImage.Height;
                newHeight = maxHeight;
            }

            Image newImage = fullsizeImage.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            fullsizeImage.Dispose();

            // Save resized picture
            return newImage;
        }
        public static Image ResizeImage(Image fullsizeImage, int newWidth, int maxHeight, bool onlyResizeIfWider)
        {
            // Prevent using images internal thumbnail
            fullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            fullsizeImage.RotateFlip(RotateFlipType.Rotate180FlipNone);

            if (onlyResizeIfWider)
            {
                if (fullsizeImage.Width <= newWidth)
                {
                    newWidth = fullsizeImage.Width;
                }
            }

            int newHeight = fullsizeImage.Height * newWidth / fullsizeImage.Width;
            if (newHeight > maxHeight)
            {
                // Resize with height instead
                newWidth = fullsizeImage.Width * maxHeight / fullsizeImage.Height;
                newHeight = maxHeight;
            }

            Image newImage = fullsizeImage.GetThumbnailImage(newWidth, newHeight, null, IntPtr.Zero);

            // Clear handle to original file so that we can overwrite it if necessary
            fullsizeImage.Dispose();

            // Save resized picture
            return newImage;
        }

    }
}
